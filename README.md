# Filelink for Dracoon

This Thunderbird AddOn makes it easy to send large attachments: It uploads the files to [Dracoon](https://www.dracoon.com)  and inserts download links into the message.

Dracoon is a trademark of Dracoon GmbH. I'm not in any way connected to that company. I can only hope they let me use their trademark in the Addon's name, because it wouldn't make any sense without it.
